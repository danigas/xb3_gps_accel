from lib.gps_mngr import GPS_Mngr
from lib.gps_decode import GPS_Decode
from lib.cmd_mngr import Cmd_Mngr
from lib.accelerometer_logic import Accelerometer
from lib.system import System
from lib.heart_beat import Heart_Beat
from gc import collect


# instances
sys = System()
accel = Accelerometer()
gps_mngr = GPS_Mngr()
gps_decode = GPS_Decode()
heart_beat = Heart_Beat()


# main loop
while True:

    # accelerometer task
    accel_interrupt = accel.task()

    # gps decode task
    gps_data = []
    if gps_decode.get_instant_data(gps_data):
        gps_mngr.set_last_valid_data(gps_data)

    # system tasks
    if accel_interrupt and gps_mngr.latched:
        sys.add_element_to_queue(gps_mngr.get_last_valid_data(), accel.get_temperature())
        gps_mngr.print_gps_data()

    sys.task()

    # gps manager task
    gps_mngr.task()

    # command manager task
    Cmd_Mngr.task(gps_mngr, accel, sys)

    # Alive led task
    heart_beat.task(gps_mngr.latched)

    collect()
