from lib.utils_misc import parse_to_32bits_msb_signed, parse_int_to_32_bit_byte_array_msb_signed


def gps_data_parser(gps_data_list):
    if len(gps_data_list) >= 8:
        parsed_params = bytearray(14)
        parsed_params[0] = gps_data_list[0] # hours
        parsed_params[1] = gps_data_list[1] # mins
        parsed_params[2] = gps_data_list[2] # secs
        parsed_params[3] = gps_data_list[5] # month
        parsed_params[4] = gps_data_list[6] # day
        parsed_params[5] = gps_data_list[7] # year
        parsed_params[6:10] = parse_int_to_32_bit_byte_array_msb_signed(int(gps_data_list[3] * 100000))
        parsed_params[10:] = parse_int_to_32_bit_byte_array_msb_signed(int(gps_data_list[4] * 100000))
        return parsed_params
    return bytearray([0, ])


def accel_params_parser(accel_threshold_array):
    return [accel_threshold_array[0], parse_to_32bits_msb_signed(accel_threshold_array, 1)]


def accel_params_encode(enabled, accel_threshold_mg):
    encoded_params = bytearray(5)
    encoded_params[0] = int(enabled)
    encoded_params[1:] = parse_int_to_32_bit_byte_array_msb_signed(accel_threshold_mg)
    return encoded_params


def accel_values_encode(accel_mg: int, temperature: int):
    ret_array = parse_int_to_32_bit_byte_array_msb_signed(accel_mg)
    ret_array.append(temperature)
    return ret_array
