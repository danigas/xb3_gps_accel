from xbee import transmit, ADDR_BROADCAST

MSG_MAX_LEN = 50


class Debug_Msg:
    def __init__(self):
        pass

    @staticmethod
    def print(msg: str):
        if msg is not None:
            nb_of_msgs = int(len(msg) / MSG_MAX_LEN)
            if len(msg) % MSG_MAX_LEN > 0:
                nb_of_msgs += 1

            for x in range(0, nb_of_msgs):
                transmit(ADDR_BROADCAST, msg[x * MSG_MAX_LEN: (x * MSG_MAX_LEN) + MSG_MAX_LEN])
            transmit(ADDR_BROADCAST, "\n")





