from time import ticks_ms
from lib.cmd_mngr import Cmd_Mngr
from lib.static_memory import static_mem_read_gps_params, static_mem_save_gps_params
from gc import enable
from lib.debug_messages import Debug_Msg


class GPS_Mngr:
    def __init__(self):
        self.send_data_offset_ms = ticks_ms()
        self.send_data_period_ms = 10000
        self.last_valid_data_list = []
        self.latched = False
        self.latch_offset = 0
        self.enable_broadcast_data = True
        enable()

        # check values from memory: Enable broadcast and broadcast period
        params_list = []
        if static_mem_read_gps_params(params_list):
            self.enable_broadcast_data = bool(params_list[0])
            self.send_data_period_ms = params_list[1] * 1000
        else:
            Debug_Msg.print("GPS: loading default params")
            static_mem_save_gps_params(self.enable_broadcast_data, int(self.send_data_period_ms/1000))

    def task(self):
        ticks_now = ticks_ms()

        # check for broadcasting data
        if self.enable_broadcast_data is True and ticks_now - self.send_data_offset_ms >= self.send_data_period_ms:
            Cmd_Mngr.send_gps_data(self.last_valid_data_list)
            self.send_data_offset_ms = ticks_now

        # if gps passes more than 2 seconds with any new valid data it's considered unlatched
        if self.latched is True and ticks_now - self.latch_offset >= self.send_data_period_ms:
            self.latched = False

    def set_params(self, enable_broadcast_data, send_gps_data_period_sec):
        if 0 < send_gps_data_period_sec < 120:
            self.send_data_period_ms = send_gps_data_period_sec * 1000
            self.enable_broadcast_data = bool(enable_broadcast_data)
            self.send_data_offset_ms = ticks_ms()
            # save params
            static_mem_save_gps_params(bool(enable_broadcast_data), send_gps_data_period_sec)
            return True
        return False

    def get_params(self):
        return [self.enable_broadcast_data, int(self.send_data_period_ms / 1000)]

    def get_last_valid_data(self):
        return self.last_valid_data_list

    def set_last_valid_data(self, gps_data: list):
        self.last_valid_data_list = gps_data
        self.latched = True
        self.latch_offset = ticks_ms()

    def print_gps_data(self):
        gps_data = self.get_last_valid_data()
        """
        if len(gps_data) == 8:
            ltc = "GPS latched"
            if not self.latched:
                ltc = "GPS not latched"
            Debug_Msg.print(ltc + ", UTC time: " + str(gps_data[0]) + ":" + str(gps_data[1]) + ":" + str(gps_data[2]))
            Debug_Msg.print("Lat:" + str(gps_data[3]) + ", Long: " + str(gps_data[4]))
        """