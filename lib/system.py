from time import ticks_ms
from lib.cmd_mngr import Cmd_Mngr
from xbee import atcmd

RECEIVE_ACK_TIMEOUT_MS = 20000
MSG_BUFF_LEN = 10

# index of columns in queue
MSG_ID_IDX = 0
GPS_DATA_IDX = 1
TEMPERATURE_IDX = 2
TIME_OFFSET_IDX = 3
MSG_STATUS_IDX = 4 # 0: not sent, 1: wait ack, 2: done (due to timeout or ack received), 0Xff: invalid


class System:
    def __init__(self):
        a_List = [0, [], 0, 0, 0xFF]  # id, gps_list , temperature, time_offset, status
        self.queue_list = []  # list of messages to send to gateway
        for x in range(0, MSG_BUFF_LEN):
            self.queue_list.append(a_List)
        self.queue_idx = 0  # index of queues
        self.msg_id = 0
        self.check_offset = 0
        self.init_system()

    def task(self):
        ticks_now = ticks_ms()
        if ticks_now - self.check_offset >= 1000: # do the following each second
            for idx in range(0, MSG_BUFF_LEN):
                # check msgs not sent
                if self.queue_list[idx][MSG_STATUS_IDX] == 0:
                    # send!!!!
                    Cmd_Mngr.send_gps_data(self.queue_list[idx][GPS_DATA_IDX], self.queue_list[idx][MSG_ID_IDX],
                                           self.queue_list[idx][TEMPERATURE_IDX])
                    self.queue_list[idx][MSG_STATUS_IDX] = 1 # waiting ACK

                elif self.queue_list[idx][MSG_STATUS_IDX] == 1 and ticks_now - self.queue_list[idx][TIME_OFFSET_IDX] >= RECEIVE_ACK_TIMEOUT_MS:
                    # resend!
                    Cmd_Mngr.send_gps_data(self.queue_list[idx][GPS_DATA_IDX], self.queue_list[idx][MSG_ID_IDX],
                                           self.queue_list[idx][TEMPERATURE_IDX])
                    self.queue_list[idx][MSG_STATUS_IDX] = 2  # resend and done
            self.check_offset = ticks_now

    def add_element_to_queue(self, gps_list: list, temperature: int):
        add_list = [self.msg_id, gps_list, temperature, ticks_ms(), 0]
        self.queue_list[self.queue_idx] = add_list

        self.msg_id += 1
        if self.msg_id == 255: # msg id up to 254
            self.msg_id = 0

        self.queue_idx += 1
        if self.queue_idx == MSG_BUFF_LEN:
            self.queue_idx = 0

    def ack_received(self, msg_id: int):
        for idx in range(0, MSG_BUFF_LEN):
            if self.queue_list[idx][MSG_ID_IDX] == msg_id:
                self.queue_list[idx][MSG_STATUS_IDX] = 2 # message ACK

    @staticmethod
    def init_system():
        # SLEEP MODE = 0 (NORMAL), 6 (MICROPYTHON)
        atcmd("SM", 0)
        # MICROPYTHON AUTOSTART = 1 (ON)
        atcmd("PS", 1)
        # UART MODE = 4 (MICROPYTHON REPL MODE) needed for debug console OR controlling a device via uart (gps for instance)
        atcmd("AP", 4)
        # TX POWER = 4 (MAX)
        atcmd("PL", 4)
        # Set internal voltage reference to 2.5V:
        atcmd("AV", 1)
        # Radio Channel
        atcmd("CH", 0x0C)
        # Encryption disabled
        atcmd("EE", 0)
        # Encryption key
        #atcmd("KY", bytearray([0x11, 0xAA, 0x22, 0xBB, 0x33, 0xCC]))
        # Save parameters
        atcmd("WR")
