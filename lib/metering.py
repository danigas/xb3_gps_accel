from machine import Pin, ADC
from lib.utils_misc import parseTo16BitsMsb, parseIntTo16BitByteArrayMsb


class Metering(object):
    BITMASK__VOLTAGE = 0x0001

    def __init__(self):
        self.adcPin = Pin("D2", Pin.ANALOG)
        self.adcBatt = ADC("D2")

    def encode_tx(self):
        encoded_data = bytearray()
        encoded_data.extend(parseIntTo16BitByteArrayMsb(self.BITMASK__VOLTAGE))
        vbat = self.read_battery_voltage()
        #print("Metering: Battery Voltage %.4f\n" % vbat)

        encoded_data.extend(parseIntTo16BitByteArrayMsb(int(vbat * 100)))
        return encoded_data

    def check_rx(self, received_data):
        if len(received_data) == 2:
            bitmask = parseTo16BitsMsb(received_data, 0)
            if bitmask & self.BITMASK__VOLTAGE == self.BITMASK__VOLTAGE:
                return True
        return False

    def read_battery_voltage(self):
        return self.adcBatt.read() * 5 / 4095
