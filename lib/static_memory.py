from lib.utils_misc import checksum_16bits
from lib.i2c_mem import I2C_Mem
from lib.static_memory_config import Static_Mem_Cfg
from lib.parser import accel_params_parser, accel_params_encode


def static_mem_save_gps_params(enable_broadcast: bool, broadcast_period_sec: int):
    """
    Memory organization for gps parameters (address 0 means address offset):
    address 0: enable broadcast gps data 1 byte
    address 1: broadcast period secs 1 byte
    address 2: checksum 2 bytes
    total 4 bytes
    """
    bytes_to_save = bytearray([int(enable_broadcast), broadcast_period_sec])
    return _append_checksum_and_write(bytes_to_save, Static_Mem_Cfg.GPS_PARAMS_ADDRESS)


def static_mem_read_gps_params(params_list: list):
    """
    Memory organization for gps parameters (address 0 means address offset):
    address 0: enable broadcast gps data 1 byte
    address 1: broadcast period secs 1 byte
    address 2: checksum 2 bytes
    total 4 bytes
    """
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, Static_Mem_Cfg.GPS_PARAMS_ADDRESS, Static_Mem_Cfg.GPS_PARAMS_LENGTH):
        # encoding
        params_list.append(bytes_read[0])
        params_list.append(bytes_read[1])
        return True
    return False


def static_mem_save_accel_params(enabled: bool, interrupt_thresshold_mg: int):
    """
    Memory organization for accel parameters (address 0 means address offset):
    address 0: interrupt enabled/disabled 1 byte
    address 1: acceleration threshold for interrupt in milli g 4 byte
    address 5: checksum 2 bytes
    total 7 bytes
    """
    return _append_checksum_and_write(accel_params_encode(enabled, interrupt_thresshold_mg),
                                      Static_Mem_Cfg.ACCEL_PARAMS_ADDRESS)


def static_mem_read_accel_params(params_list: list):
    """
    Memory organization for accel parameters (address 0 means address offset):
    address 0: interrupt enabled/disabled 1 byte
    address 1: acceleration threshold for interrupt in milli g 4 byte
    address 5: checksum 2 bytes
    total 7 bytes
    """
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, Static_Mem_Cfg.ACCEL_PARAMS_ADDRESS, Static_Mem_Cfg.ACCEL_PARAMS_LENGTH):
        # parse
        params_list.extend(accel_params_parser(bytes_read))
        return True
    return False


def _basic_read_array(bytes_read: bytearray, address: int, length: int):
    i2c = I2C_Mem(Static_Mem_Cfg.MEMORY_SIZE_BYTES, Static_Mem_Cfg.PAGE_SIZE_BYTES)
    bytes_read.extend(i2c.read_array(address, length))
    if len(bytes_read) == length:
        check_sum_calculated = checksum_16bits(bytes_read[0:(length - 2)])
        check_sum_read = (bytes_read[length - 2] << 8) + bytes_read[length - 1]
        if check_sum_calculated == check_sum_read:
            return True
    return False


def _append_checksum_and_write(bytes_to_save: bytearray, address: int):
    check_sum = checksum_16bits(bytes_to_save)
    bytes_to_save.append(check_sum >> 8)
    bytes_to_save.append(check_sum & 0xFF)
    i2c = I2C_Mem(Static_Mem_Cfg.MEMORY_SIZE_BYTES, Static_Mem_Cfg.PAGE_SIZE_BYTES)
    return i2c.write_array(address, bytes_to_save)

