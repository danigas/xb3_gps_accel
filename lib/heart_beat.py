from machine import Pin
from time import ticks_ms


class Heart_Beat:
    def __init__(self):
        self.led_pin = Pin('D4', Pin.OUT)
        self.time_offset_ms = 0
        self.status = True
        self.led_pin.value(int(self.status))

    def task(self, flag=False):
        ticks_now = ticks_ms()
        on_time_ms = 100
        off_time_ms = 900
        if not flag:
            on_time_ms = 500
            off_time_ms = 500

        if self.status is True and ticks_now - self.time_offset_ms >= off_time_ms:
            self.status = False
            self.led_pin.value(int(self.status))
            self.time_offset_ms = ticks_now
        elif self.status is False and ticks_now - self.time_offset_ms >= on_time_ms:
            self.status = True
            self.led_pin.value(int(self.status))
            self.time_offset_ms = ticks_now
