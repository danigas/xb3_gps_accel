from lib.utils_misc import byteArrayInsertElement, checksum_16bits


class PacketOrigin:
    NOT_SET = 0
    FIELD = 1
    CURRENT_DEVICE = 2
    SERVER = 3


class PacketType:
    ACK = 1
    NACK = 2
    SCENARIO = 3


class DP1Packet():
    """
    DP1Packet is a container for packet data.
    Contains Source, Destination, Command Id, Arguments, Packet Type and Label.
    """

    __tx_label = 1
    DP1_DELIMITER = 0x11
    ESCAPE_CHAR = 0x1A
    MAX_DP1_PACKET_LEN = 256
    MIN_DP1_PACKET_LEN = 21

    __BYTES_OF_PARTIAL_DP1_PACKET = bytearray()

    def __init__(
            self,
            src,
            dest,
            cmd,
            args=(),
            packet_type=PacketType.ACK,
            origin=PacketOrigin.NOT_SET,
            is_dest_group=False,
            dest_node=0,
            retransmit=1,
            label=-1,
    ):

        self.src = src
        self.dest = dest
        self.cmd = cmd
        self.args = args
        self.packet_type = packet_type
        self.origin = origin
        self.is_dest_group = is_dest_group
        self.dest_node = dest_node
        self.retransmit = retransmit
        self.label = label

    def encode(self):
        """
        Create a DP1 message from a DP1Packet object

        :returns: bytearray -- an encoded tx message
        """
        encoded_packet = bytearray()

        encoded_packet.append(0xA0)  # PROTOCOL DEFAULT BYTES
        encoded_packet.append(0x0A)  # PROTOCOL DEFAULT BYTES
        encoded_packet.append(0x82)  # PROTOCOL DEFAULT BYTES
        args_len = len(self.args)
        encoded_packet.append((args_len + 19) & 0xFF)  # PACKET LENGTH = FIXED LENGTH + ARGS LENGTH
        encoded_packet.append(0xA0)  # UNACKNOWLEDGED PACKET, NORMAL PRIORITY (OLD PROTOCOL)
        encoded_packet.append(0x01)  # PROTOCOL ID

        if 1 <= self.label <= 15:
            DP1Packet.__tx_label = self.label  # GET TX PACKET TX LABEL IF IT IS VALID
        # We assume the label is valid before encoding because it is at 1 by default and then we check if it respects limits
        encoded_packet.append((DP1Packet.__tx_label << 4) & 0xF0)

        DP1Packet.__tx_label += 1  # INCREMENT INTERNAL TX LABEL
        # The label value must be lower than 15
        if DP1Packet.__tx_label > 15 or DP1Packet.__tx_label < 1:
            DP1Packet.__tx_label = 1

        if self.is_dest_group:
            destination = self.dest << 16
            destination |= self.dest_node
            destination &= 0x7FFFFFFF
        else:
            destination = self.dest
            destination |= 0x80000000  # ADDING MSB BIT AS 1 TO INDICATE SN DESTINATION

        encoded_packet.append(destination & 0xFF)
        encoded_packet.append((destination >> 8) & 0xFF)
        encoded_packet.append((destination >> 16) & 0xFF)
        encoded_packet.append((destination >> 24) & 0xFF)

        source = self.src
        source |= 0x80000000
        encoded_packet.append(source & 0xFF)
        encoded_packet.append((source >> 8) & 0xFF)
        encoded_packet.append((source >> 16) & 0xFF)
        encoded_packet.append((source >> 24) & 0xFF)

        encoded_packet.append((len(self.args) + 5) & 0xFF)

        encoded_packet.append(0x80)  # NO SEGMENTATION IN THE PACKET

        # CREATING DATA HEADER COMPOSED BY MSO / GLOBAL / VALUE BITS AND 2 MSB BITS OF CMD ID
        if self.packet_type == PacketType.SCENARIO:
            data_header = 0x0C  # VALUE = 0 / MSO = 1 / GLOBAL = 1
        elif self.packet_type == PacketType.NACK:
            data_header = 0x14  # VALUE = 1 / MSO = 0 / GLOBAL = 1
        else:  # BY DEFAULT WE WILL SET AS GENBASE ACK
            data_header = 0x1C  # VALUE = 1 / MSO = 1 / GLOBAL = 1

        data_header |= (self.cmd >> 8) & 0x03  # ADDING 2 MSO BITS OF COMMAND ID TO THIS BYTE
        encoded_packet.append(data_header)

        encoded_packet.append(self.cmd & 0xFF)

        encoded_packet.append(0x15)  # ARGUMENTS TYPE ALWAYS = ARRAY OF BYTES

        encoded_packet.append(len(self.args) & 0xFF)

        for arg in self.args:
            encoded_packet.append(int(arg))

        checksum = checksum_16bits(encoded_packet)
        encoded_packet.append(checksum & 0xFF)
        encoded_packet.append((checksum >> 8) & 0xFF)

        self.__insert_escape_chars(encoded_packet)

        self.__add_delimiters(encoded_packet)

        return encoded_packet

    @staticmethod
    def __insert_escape_chars(encoded_packet):
        index = 1
        while index < len(encoded_packet):

            if encoded_packet[index] == DP1Packet.ESCAPE_CHAR or encoded_packet[index] == DP1Packet.DP1_DELIMITER:
                char_to_escape = encoded_packet[index]
                byteArrayInsertElement(index + 1, encoded_packet, char_to_escape + 0x10)
                encoded_packet[index] = DP1Packet.ESCAPE_CHAR
                index += 1

            index += 1

    @staticmethod
    def __add_delimiters(encoded_packet):
        byteArrayInsertElement(0, encoded_packet, DP1Packet.DP1_DELIMITER)
        encoded_packet.append(DP1Packet.DP1_DELIMITER)  # ADDING END DELIMITER

    @classmethod
    def decode(cls, stream):
        """
        Try to transform a rf payload into a DP1 packet
        :param stream: byte array that contains the payload of a Packet. To send/receive RF packet, any radio must tell
        the destination radio and some other information, along with the data to send to that other radio. So in our case, the
        stream is the data that the other radio sent to us (the gateway)
        :returns: DP1Packet -- the decoded DP1 Packet
        """
        possible_dp1_packets = DP1Packet.extract_dp1_bytes_from_stream(stream)

        valid_dp1_packets = []
        for dp1_bytes in possible_dp1_packets:
            if len(dp1_bytes) < DP1Packet.MIN_DP1_PACKET_LEN or len(dp1_bytes) > DP1Packet.MAX_DP1_PACKET_LEN:
                continue

            unescaped_dp1_bytes = DP1Packet.remove_escape_chars(dp1_bytes)
            if unescaped_dp1_bytes is None:
                continue

            is_checksum_valid = DP1Packet.validate_checksum(unescaped_dp1_bytes, dp1_bytes, stream)
            if not is_checksum_valid:
                continue

            label = (unescaped_dp1_bytes[6] >> 4) & 0x0F

            if unescaped_dp1_bytes[10] & 0x80 > 0:  # The destination is an address
                dest = unescaped_dp1_bytes[7]
                dest |= unescaped_dp1_bytes[8] << 8
                dest |= unescaped_dp1_bytes[9] << 16
                dest |= unescaped_dp1_bytes[10] << 24
                dest &= 0x7FFFFFFF
                dest = dest
                is_dest_group = False
                dest_node = (
                    0
                )  # We set the variable to avoid exception when passing a variable that doesn't exist to create DP1 packet

            else:  # The destination is a group
                dest_node = unescaped_dp1_bytes[7] | unescaped_dp1_bytes[8] << 8
                dest_node &= 0x7FFF
                dest_group = unescaped_dp1_bytes[9] | unescaped_dp1_bytes[10] << 8
                dest_group &= 0x7FFF
                dest = dest_group
                is_dest_group = True
                dest_node = dest_node

            src = unescaped_dp1_bytes[11]
            src |= unescaped_dp1_bytes[12] << 8
            src |= unescaped_dp1_bytes[13] << 16
            src |= unescaped_dp1_bytes[14] << 24
            src &= 0x7FFFFFFF

            # Byte index 16 = packet segmentation -> not used
            # (MSO/VALUE/GLOBAL BITS) -> packet type
            value_bit = unescaped_dp1_bytes[17] & (1 << 4)
            mso_bit = unescaped_dp1_bytes[17] & (1 << 3)
            global_bit = unescaped_dp1_bytes[17] & (1 << 2)

            packet_type = PacketType.ACK  # 1 1 1
            if value_bit and not mso_bit and global_bit:
                packet_type = PacketType.NACK
            elif not value_bit and mso_bit and global_bit:
                packet_type = PacketType.SCENARIO

            # Getting the command ID
            cmd = (unescaped_dp1_bytes[17] & 0x03) << 8
            cmd |= unescaped_dp1_bytes[18]

            # NORMALLY THE VALUE WILL BE 3 FOR PACKETS WITHOUT ARGS, OR 5 AND MORE FOR PACKETS WITH PARAMETERS
            # IF PACKET HAS ARGS, 2 MORE BYTES WILL BE USED TO TELL THAT THERE ARE ARGS AND THEIR LENGTH
            args = tuple()
            if unescaped_dp1_bytes[15] >= 5 and len(unescaped_dp1_bytes) >= DP1Packet.MIN_DP1_PACKET_LEN + 2:

                # BYTE index 19 (UNUSED) is the argument type. The type is always a byte array. (0X15)
                # BYTE index 20 is the argument length
                if 21 + unescaped_dp1_bytes[20] > len(unescaped_dp1_bytes):
                    continue
                else:
                    args = tuple(unescaped_dp1_bytes[21: 21 + unescaped_dp1_bytes[20]])  # Packet args must be a list

            rx_packet = DP1Packet(
                src=src,
                dest=dest,
                cmd=cmd,
                args=args,
                packet_type=packet_type,
                origin=PacketOrigin.FIELD,
                is_dest_group=is_dest_group,
                label=label,
                dest_node=dest_node,
            )
            valid_dp1_packets.append(rx_packet)

        return valid_dp1_packets

    @staticmethod
    def extract_dp1_bytes_from_stream(stream):
        """
        We don't control the length of the stream, so there could be more than one section of the stream that
        contains a valid dp1 packet.
        When the bytes between 2 delimiters (0x11) have a valid length, we add them to
        a list of the bytes that could make a dp1 packet. Each byte array is then checked to ensure its
        validity
        :param stream: a bytearray of any length that we don't control.
        :return: A list of bytearrays that could make valid dp1 packets
        """
        possible_dp1_packets = []  # List of byte arrays that can be dp1 packets
        try:
            for byte in stream:

                if byte != DP1Packet.DP1_DELIMITER:

                    if len(DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET) < DP1Packet.MAX_DP1_PACKET_LEN:
                        DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET.append(byte)

                else:  # The current byte IS a dp1 delimiter, we want to evaluate what to do with the packet currently being parsed

                    if len(DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET) >= DP1Packet.MIN_DP1_PACKET_LEN:
                        possible_dp1_packets.append(DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET)

                    DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET = bytearray()  # Clear byte array

            return possible_dp1_packets

        except Exception as exc:
            # logging.exception("Extraction Failed")
            DP1Packet.__BYTES_OF_PARTIAL_DP1_PACKET = bytearray()  # Clear byte array
            raise exc

    @staticmethod
    def check_packet_structure(dp1_array_no_esc):
        """
        Check if the packet contains valid fixed data and length bytes

        :param dp1_array_no_esc: byte array containing the DP1 message
        :returns: bool - true if the packet's structure is valid
        """
        return (
                dp1_array_no_esc[0] == 0xA0
                and dp1_array_no_esc[1] == 0x0A  # OLD PROTOCOL DEFAULTS
                and (
                        dp1_array_no_esc[2] == 0x81 or dp1_array_no_esc[2] == 0x82 or dp1_array_no_esc[2] == 0x83
                )  # OLD PROTOCOL DEFAULTS
                and  # /OLD PROTOCOL DEFAULTS
                # LENGTH FROM HERE UNTIL THE END OF PACKET (INCLUDES CHECKSUM)
                dp1_array_no_esc[3] == len(dp1_array_no_esc) - 4
                and dp1_array_no_esc[4] == 0xA0
                and dp1_array_no_esc[5] == 0x01  # OLD PROTOCOL DEFAULTS
                and dp1_array_no_esc[6] & 0x0F == 0  # OLD PROTOCOL DEFAULTS
                and dp1_array_no_esc[6] & 0xF0 > 0  # The packet label's low nibble must be equal to 0
                and dp1_array_no_esc[14] & 0x80 > 0  # The packet label's high nibble must be greater than 0
                and  # The source must be an address
                # LENGTH FROM HERE UNTIL THE END OF PACKET (EXCLUDES CHECKSUM)
                dp1_array_no_esc[15] == len(dp1_array_no_esc) - 18
        )

    @staticmethod
    def remove_escape_chars(dp1_bytes):
        """
        When a device sends a packet, if a value inside of it is 0x11 or 0x1A, it must be escaped so the receiver
        (the gateway) doesn't believe that one packet is two.
        :param dp1_bytes: Bytearray between 2 characters 0x11
        :return: A bytearray with escape characters de-escaped
        """
        try:
            modified_payload = bytearray()

            previous_char_is_esc = False
            for byte in dp1_bytes:

                # If the character is an escape char, don't add it to the array to return and set true so next byte will be modified
                if byte == DP1Packet.ESCAPE_CHAR:
                    previous_char_is_esc = True

                else:
                    if previous_char_is_esc:
                        modified_payload.append(byte - 0x10)
                        previous_char_is_esc = False

                    else:
                        modified_payload.append(byte)

            return modified_payload
        except Exception:
            #print("Remove Escape Chars Failed")
            pass

    @staticmethod
    def validate_checksum(de_escaped_dp1_payload, initial_dp1_bytes, stream):
        """

        :param de_escaped_dp1_payload: bytes on which escape characters have been de-escaped
        :param initial_dp1_bytes: bytes that were between 2 characters 0x11 before we extracted them and de-escpaed the packet
        :param stream: byte array that the decoder receives initially, before any parsing happens
        :return: True if the checksum is valid, False if not
        """
        is_packet_valid = DP1Packet.check_packet_structure(de_escaped_dp1_payload)
        if not is_packet_valid:
            return False

        calc_checksum = 0

        for index in range(0, len(de_escaped_dp1_payload) - 2):
            calc_checksum += de_escaped_dp1_payload[index]

        calc_checksum &= 0xFFFF

        received_checksum = de_escaped_dp1_payload[len(de_escaped_dp1_payload) - 2]
        received_checksum |= de_escaped_dp1_payload[len(de_escaped_dp1_payload) - 1] << 8
        received_checksum &= 0xFFFF

        return True
