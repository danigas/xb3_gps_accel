from machine import I2C
from time import sleep_ms


class I2C_Mem():
    def __init__(self, mem_size_bytes, page_size_bytes=16):
        self.is_present = False
        self.mem_size_bytes = mem_size_bytes
        self.page_size_bytes = page_size_bytes
        self.i2c = I2C(1, freq=100000)  # create instance for I2C peripheral at frequency of 100kHz
        self.device_addresses = self.get_device_addresses()
        self.addressing_bits = 8
        self.mem_addr_mask = 0xFF
        if len(self.device_addresses) > 0: # devices found
            self.is_present = True
            device_mem_partition_bytes = self.mem_size_bytes / len(self.device_addresses)
            if device_mem_partition_bytes > 256:  # if partition is bigger than 256 bytes, addressing must take 16 bits (2 bytes)
                self.addressing_bits = 16
                self.mem_addr_mask = 0xFFFF

    def write_array(self, mem_addr, array):
        write_ok = False
        if self.is_present:
            if mem_addr % self.page_size_bytes == 0:
                length = len(array)
                nb_of_pages = int(length / self.page_size_bytes)
                if length % self.page_size_bytes > 0:
                    nb_of_pages += 1
                # Send packets of page_size bytes length
                for page in range(0, nb_of_pages):
                    send_array = bytearray()
                    new_offset = mem_addr + page * self.page_size_bytes
                    device_addr = self.get_device_address_from_mem_addr(new_offset)
                    new_offset &= self.mem_addr_mask
                    send_array.extend(
                        array[(page * self.page_size_bytes): (page * self.page_size_bytes + self.page_size_bytes)])
                    self.i2c.writeto_mem(device_addr, new_offset, send_array, addrsize=self.addressing_bits)
                    sleep_ms(1)
                    write_ok = True
            else:
                #print("mem address is not the beginning of a page")
                pass
        return write_ok

    def read_array(self, mem_addr, length):
        read_bytes = bytearray()
        if self.is_present:
            device_addr = self.get_device_address_from_mem_addr(mem_addr)
            mem_addr &= self.mem_addr_mask
            read_bytes = self.i2c.readfrom_mem(device_addr, mem_addr, length, addrsize=self.addressing_bits)
            sleep_ms(1)
        return read_bytes

    def get_device_addresses(self):
        memory_device_address_mask = 0x50  # Most I2C memories has this address
        scan = self.i2c.scan()
        device_addresses = list()
        for addr in scan:
            if (addr & memory_device_address_mask) == 0x50:
                device_addresses.append(addr)
        return device_addresses

    def get_device_address_from_mem_addr(self, mem_address):
        device_mem_partition = self.mem_size_bytes / len(self.device_addresses)
        nb_of_partitions = len(self.device_addresses)
        for idx in range(0, nb_of_partitions):
            if mem_address < device_mem_partition * (1 + idx):
                return self.device_addresses[idx]
        return 0
