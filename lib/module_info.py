from xbee import atcmd


class Module_Info:
    def __init__(self):
        pass

    @staticmethod
    def discover_encoding():
        # TODO: UPDATE THE FOLLOWING WITH ACTUAL VALUES:
        FW_VERSION = bytearray([1, 0])
        HW_VERSION = bytearray([1, 0])
        FW_ID = bytearray([0x00, 0x4A])
        DESCRIPTION = bytearray(b"_GPS_+_ACCEL_device_")

        data_encoded = bytearray()
        uid = Module_Info.get_uid()
        data_encoded[0:0 + len(uid)] = uid
        data_encoded[8:8 + len(FW_VERSION)] = FW_VERSION
        data_encoded[10:10 + len(HW_VERSION)] = HW_VERSION
        data_encoded[12:12 + len(FW_ID)] = FW_ID
        data_encoded[14:14 + len(DESCRIPTION)] = DESCRIPTION
        return data_encoded

    @staticmethod
    def get_own_address():
        return int.from_bytes(atcmd("SL"), 'big', False)  # 4 bytes unique address

    @staticmethod
    def get_uid():
        return atcmd("SH") + atcmd("SL")
