from sys import stdin, stdout
from lib.utils_misc import checksum_Xor_8bits
from xbee import atcmd
from time import sleep_ms
from debug_messages import Debug_Msg


class GPS_Decode:
    START_SYNC = 0
    READ_END_OF_STRING = 1

    def __init__(self):
        self.decode_status = self.START_SYNC
        self.string_read = ""
        self.init_L80r()

    def get_instant_data(self, return_list):
        return_value = False

        # read all bytes available from serial port (only way to do it unblocking)
        raw_read = self.safe_serial_read()

        if raw_read is not None:
            if self.decode_status is self.START_SYNC:
                # search for first char of string '$'
                # search for end char '\n'
                index_of_dollar = None
                index_of_NL = None
                try:
                    index_of_dollar = raw_read.index('$')
                    index_of_NL = raw_read.index('\n') # last char
                except:
                    pass

                if index_of_dollar is not None and index_of_NL is not None and index_of_dollar < index_of_NL:
                    # at least one complete message was read
                    self.string_read = raw_read[index_of_dollar: index_of_NL]
                    # decode:
                    if Decode_Fcn.decode(return_list, self.string_read):
                        return_value = True
                    self.string_read = ""

                    # check remaining chars if read more than one complete message
                    try:
                        index_of_dollar = raw_read.index('$')
                        self.string_read = raw_read[index_of_dollar:]
                        self.decode_status = self.READ_END_OF_STRING
                    except:
                        pass
                elif index_of_dollar is not None:
                    # a part of message was read
                    self.string_read = raw_read[index_of_dollar:]
                    self.decode_status = self.READ_END_OF_STRING

            elif self.decode_status is self.READ_END_OF_STRING:
                # search for end char '\n'
                index_of_NL = None
                try:
                    index_of_NL = raw_read.index('\n')
                except:
                    pass

                if index_of_NL is not None:
                    # new line read, the message is complete now
                    self.string_read += raw_read[: index_of_NL]
                    self.decode_status = self.START_SYNC
                    # DECODE HERE!!!
                    if Decode_Fcn.decode(return_list, self.string_read):
                        return_value = True
                    self.string_read = ""

                    # check remaining chars
                    try:
                        index_of_dollar = raw_read.index('$')
                        self.string_read = raw_read[index_of_dollar:]
                        self.decode_status = self.READ_END_OF_STRING
                    except:
                        pass

                else:
                    self.string_read += raw_read

        return return_value

    @staticmethod
    def safe_serial_read():
        try:
            raw_read = stdin.read()
            if raw_read is not None and len(raw_read) >= 25:
                # discard long read as this brings memory problems
                return None
            elif raw_read is not None and len(raw_read) < 25:
                return raw_read
            else:
                return None

        except MemoryError:
            #print("Memory problems! stdin.read()")
            Debug_Msg.print("Gps Decode: Memory problems! Reset...")
            atcmd('FR')

    @staticmethod
    def init_L80r():
        try:
            # set position fix interval 500 ms
            stdout.write("$PMTK220,500*2B\r\n")
            sleep_ms(10)
            stdin.read()

            # set only GPRMS msgs as output, once each position fix (once each 500ms)
            stdout.write("$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n")
            sleep_ms(10)
            stdin.read()  # read ack, discard
        except:
            pass


class Decode_Fcn:
    def __init__(self):
        pass

    @staticmethod
    def decode(param_list: list, string: str):
        if Decode_Fcn.check_integrity(string):
            comma_array = Decode_Fcn.get_comma_array(string)
            list_of_params = Decode_Fcn.get_list_of_params(string, comma_array)
            Decode_Fcn.convert_params_to_list(list_of_params, param_list)
            return True

    @staticmethod
    def check_integrity(string):
        # check msg header
        if string[0: 6] == "$GPRMC" and len(string) > 60:
            # checksum
            check_sum_read = int(string[len(string) - 2:], 16)
            check_sum_calc = checksum_Xor_8bits(bytearray(string[1: len(string) - 3]))
            if check_sum_read == check_sum_calc:
                return True
        return False

    @staticmethod
    def get_comma_array(string):
        # create an array whose elements are the indexes pointing ',' elements in string
        comma_idx_array = bytearray()  # gprmc message has 12 commas
        idx = 0
        run = True
        while run:
            try:
                index = string.index(',', idx)
                comma_idx_array.append(index)
                idx = index + 1
            except:
                run = False
        return comma_idx_array

    @staticmethod
    def get_list_of_params(string, comma_idx_array):
        # separate params in a list (elements are strings)
        all_params = []
        try:
            for idx in range(0, len(comma_idx_array)):
                start_idx = comma_idx_array[idx] + 1  # next element after the comma
                if idx == len(comma_idx_array) - 1:  # last element
                    end_idx = len(string) - 4
                else:
                    end_idx = comma_idx_array[idx + 1]
                param_string = string[start_idx: end_idx]
                all_params.append(param_string)
        except:
            pass
        return all_params

    @staticmethod
    def convert_params_to_list(all_params, param_list):
        # from params_list, convert required string parameters (elements) into values
        param_list.append(int(all_params[0][0:2]))
        param_list.append(int(all_params[0][2:4]))
        param_list.append(int(all_params[0][4:6]))
        latitude = int(all_params[2][0:2]) + float(all_params[2][2:]) / 60
        if all_params[3] == 'S':
            latitude *= -1
        param_list.append(latitude)
        longitude = int(all_params[4][0:3]) + float(all_params[4][3:]) / 60
        if all_params[5] == 'W':
            longitude *= -1
        param_list.append(longitude)
        param_list.append(int(all_params[8][0:2]))
        param_list.append(int(all_params[8][2:4]))
        param_list.append(int(all_params[8][4:]))
