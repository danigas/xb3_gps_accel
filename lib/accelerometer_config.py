from machine import I2C
from time import sleep_ms
from lib.debug_messages import Debug_Msg


class Accel_Config:
    def __init__(self):
        pass

    @staticmethod
    def accel_init():
        if Accel_Config.init_registers():
            return True
        return False

    @staticmethod
    def write_register(register_addr: int, data_byte: int):
        i2c = I2C(1, freq=100000)
        sleep_ms(1)
        write_array = bytearray([register_addr, data_byte])
        try:
            # DEVICE ADDRES = 0X19
            if i2c.writeto(0X19, write_array) == len(write_array):
                return True
        except:
            # something where wrong: for instance operation timed out
            Debug_Msg.print("Accel: write operation failed. Reg Address: " + hex(register_addr))

        return False

    @staticmethod
    def read_register(register_addr: int):
        ret_val = None
        i2c = I2C(1, freq=100000)
        sleep_ms(1)
        write_byte = bytearray([register_addr,])
        try:
            # DEVICE ADDRES = 0X19
            i2c.writeto(0X19, write_byte, False)
            bytes_read = i2c.readfrom(0X19, 1, True)
            if len(bytes_read) >= 1:
                ret_val = bytes_read[0]
        except:
            # something where wrong: for instance operation timed out
            Debug_Msg.print("Accel: read operation failed. Reg Address: " + hex(register_addr))
        return ret_val

    @staticmethod
    def init_registers():
        # CTRL REG 1 ADDR = 0X20, VALUE = 0b01110100
        if Accel_Config.write_register(0x20, 0b01110100):
            # CTRL REG 2 ADDR = 0X21, VALUE = 0b00000000
            if Accel_Config.write_register(0x21, 0b00000000):
                # CTRL REG 4 ADDR = 0X23, VALUE = 0b10011000
                if Accel_Config.write_register(0x23, 0b10011000):
                    # CTRL REG 5 ADDR = 0X24, VALUE = 0b00001000
                    if Accel_Config.write_register(0x24, 0b00001000):
                        # CTRL REG 6 ADDR = 0X25, VALUE = 0b00001000
                        if Accel_Config.write_register(0x25, 0b00000010):
                            # TEMPERATURE CONFIG REG ADDR = 0X1F, VALUE = 0b11000000
                            if Accel_Config.write_register(0X1F, 0b11000000):
                                # INT 1 CFG REG ADDR = 0X30, VALUE = 0b00100000
                                if Accel_Config.write_register(0X30, 0b00100000):
                                    # INT 1 DURATION REG ADDR = 0X33, VALUE = 0b00010100
                                    if Accel_Config.write_register(0X33, 0b00010100):
                                        return True
        return False

    @staticmethod
    def get_temperature():
        temperature = 0x1F
        # OUT TEMP L REG ADDR = 0X0C
        temp_lsb = Accel_Config.read_register(0X0C)
        # OUT TEMP H REG ADDR = 0X0D
        temp_msb = Accel_Config.read_register(0X0D)
        if temp_msb is not None and temp_lsb is not None:
            temperature = (temp_msb << 1) + (temp_lsb >> 7)
        return temperature

    @staticmethod
    def check_interrupt_z_axis():
        # INT 1 SOURCE REG ADDR = 0X31
        int1_register_value = Accel_Config.read_register(0X31)
        # INT1 SRC REGISTER VALUE
        INT1_SRC_VALUE_Z_AXIS_HIGH = 0b01100000
        if int1_register_value is not None and \
                int1_register_value & INT1_SRC_VALUE_Z_AXIS_HIGH == INT1_SRC_VALUE_Z_AXIS_HIGH:
            return True
        return False

    @staticmethod
    def enable_disable(on_off: bool):
        ctrl3_reg_value = 0
        if on_off:
            # CTRL REG 3 ADDR = 0X22, VALUE = 0b00001000, VALUE = 0b01000000
            ctrl3_reg_value = 0b01000000
        if Accel_Config.write_register(0X22, ctrl3_reg_value):
            # INT 1 SOURCE REG ADDR = 0X31
            Accel_Config.read_register(0X31)
        # TODO: PUT DEVICE IN STAND-BY MODE
            return True
        return False

    @staticmethod
    def set_interrupt_threshold(new_accel_threshold_mg):
        accel_threshold_reg_val = Accel_Config.calculate_accel_threshold(new_accel_threshold_mg)
        # INT 1 THRESHOLD REG ADDR = 0X32
        if Accel_Config.write_register(0X32, accel_threshold_reg_val):
            return True
        return False

    @staticmethod
    def calculate_accel_threshold(accel_threshold_mg: int):
        # MAX VALUE FOR INTERRUPT THRESHOLD
        MAX_Z_ACCEL_THRESHOLD_MG = 4000
        if accel_threshold_mg > MAX_Z_ACCEL_THRESHOLD_MG:
            accel_threshold_mg = MAX_Z_ACCEL_THRESHOLD_MG
        return int(accel_threshold_mg / 32) # 32mg as full scale is set to 4g




