from lib.dp1_constants import DP1Constants
from lib.utils_misc import delay_random
from lib.parser import gps_data_parser, accel_params_parser, accel_params_encode
from lib.dp1_packet import DP1Packet
from xbee import transmit, receive, ADDR_BROADCAST
from lib.module_info import Module_Info
from lib.metering import Metering


class Cmd_Mngr:
    def __init__(self):
        pass

    @staticmethod
    def task(gps_instance=None, accel_instance=None, sys_instance=None, other_class_instances=None):
        # read radio receive buffer
        dp1Pkt = Comm_Mngr.receive_DP1()
        if dp1Pkt is not None:
            # Discover
            if dp1Pkt.cmd == DP1Constants.DEVICE__DISCOVER:
                arguments = bytearray(dp1Pkt.args)
                random_delay = 0
                if len(arguments) > 0:
                    random_delay = arguments[0]
                Cmd_Mngr.send_discover_answer(random_delay, dp1Pkt)

            # GPS COMMANDS
            elif dp1Pkt.cmd == DP1Constants.PARAMS__SET_GPS_PARAMS:
                if gps_instance is not None and len(dp1Pkt.args) > 1:
                    if gps_instance.set_params(int(dp1Pkt.args[0]), int(dp1Pkt.args[1])):
                        Comm_Mngr.send_ok_answer(dp1Pkt, DP1Constants.PARAMS__SET_GPS_PARAMS)

            elif dp1Pkt.cmd == DP1Constants.PARAMS__GET_GPS_PARAMS:
                if gps_instance is not None:
                    params_list = gps_instance.get_params()
                    argument = bytearray([int(params_list[0]), int(params_list[1])])
                    Comm_Mngr.send_DP1(dp1Pkt, DP1Constants.PARAMS__GET_GPS_PARAMS, argument)

            elif dp1Pkt.cmd == DP1Constants.DEVICE__GET_GPS_INFO:
                if sys_instance is not None and len(dp1Pkt.args) > 0:
                    # it's an ACK
                    sys_instance.ack_received(dp1Pkt.args[0])
                elif gps_instance is not None and len(dp1Pkt.args) == 0:
                    Cmd_Mngr.send_gps_data(gps_instance.get_last_valid_data(), dp1Pkt=dp1Pkt)

            # ACCELEROMETER COMMANDS
            elif dp1Pkt.cmd == DP1Constants.PARAMS__SET_ACCEL_PARAMS:
                if accel_instance is not None and len(dp1Pkt.args) >= 5:
                    [enabled, accel_mg] = accel_params_parser(bytearray(dp1Pkt.args))
                    if accel_instance.set_params(enabled, accel_mg):
                        # save params
                        Comm_Mngr.send_ok_answer(dp1Pkt, DP1Constants.PARAMS__SET_ACCEL_PARAMS)

            elif dp1Pkt.cmd == DP1Constants.PARAMS__GET_ACCEL_PARAMS:
                if accel_instance is not None:
                    [enabled, accel_mg] = accel_instance.get_params()
                    argument = accel_params_encode(enabled, accel_mg)
                    Comm_Mngr.send_DP1(dp1Pkt, DP1Constants.PARAMS__GET_GPS_PARAMS, argument)

            elif dp1Pkt.cmd == DP1Constants.METERING__GET_METERING:
                metering = Metering()
                arguments = metering.encode_tx()
                Comm_Mngr.send_DP1(dp1Pkt, DP1Constants.METERING__GET_METERING, arguments)
                
    @staticmethod
    def send_gps_data(gps_data: list, idx=None, temp=None, dp1Pkt=None):
        arguments = bytearray()
        arguments.extend(gps_data_parser(gps_data))

        if temp is not None:
            arguments.append(temp)
        else:
            arguments.append(0xFF)
        if idx is not None:
            arguments.append(idx)
        else:
            arguments.append(0xFF)

        Comm_Mngr.send_DP1(dp1Pkt, DP1Constants.DEVICE__GET_GPS_INFO, arguments)

    @staticmethod
    def send_discover_answer(random_delay_sec: int, dp1Pkt=None):
        if 0 < random_delay_sec <= 30:
            delay_random(random_delay_sec * 1000)
        Comm_Mngr.send_DP1(dp1Pkt, DP1Constants.DEVICE__DISCOVER, Module_Info.discover_encoding())


class Comm_Mngr:
    def __init__(self):
        pass

    @staticmethod
    def receive_DP1():
        message_just_received = receive()
        if message_just_received is not None:
            dp1Pkts = DP1Packet.decode(message_just_received['payload'])  # this returns a tuple of DP1 msgs (bytearray)
            if dp1Pkts is not None and len(dp1Pkts) > 0:
                # print("Comm Manager: %d Received packets, Destination Address is %d, Command is %d\n" % (len(dp1Pkts),dp1Pkts[0].dest,dp1Pkts[0].cmd))
                # Execute only if it's a Broadcast (dest = 0) or an Unicast (dest = OWN_ADDRESS) message
                if dp1Pkts[0].dest == 0 or dp1Pkts[0].dest == Module_Info.get_own_address():
                    return dp1Pkts[0]
        return None

    @staticmethod
    def send_DP1(dp1_pkt: DP1Packet, cmd: int, arguments: bytearray):
        if dp1_pkt is None:
            label = 0
            dest = 0
        else:
            label = dp1_pkt.label
            dest = dp1_pkt.src

        dp1ToSend = DP1Packet(Module_Info.get_own_address(), dest, cmd, tuple(arguments), 2, 1, False, 1, 1, label)
        msgDp1_bytes = dp1ToSend.encode()
        transmit(ADDR_BROADCAST, msgDp1_bytes)

    @staticmethod
    def send_ok_answer(dp1pkt, command):
        answer = bytearray([0x01, ])
        Comm_Mngr.send_DP1(dp1pkt, command, answer)