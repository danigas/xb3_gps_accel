from machine import Pin
from lib.static_memory import static_mem_read_accel_params, static_mem_save_accel_params
from lib.accelerometer_config import Accel_Config
from lib.debug_messages import Debug_Msg


class Accelerometer:
    def __init__(self):
        self.accel_threshold_interrupt_mg = 2000
        self.enabled = True
        self.int1_pin = Pin('D3', Pin.IN, Pin.PULL_UP)

        if Accel_Config.accel_init():
            # check values in memory
            params_list = []
            if static_mem_read_accel_params(params_list):
                self.enabled = params_list[0]
                self.accel_threshold_interrupt_mg = params_list[1]
            else:
                Debug_Msg.print("Accel: loading default params")
                static_mem_save_accel_params(self.enabled, self.accel_threshold_interrupt_mg)

            Accel_Config.enable_disable(self.enabled)
            Accel_Config.set_interrupt_threshold(self.accel_threshold_interrupt_mg)


    # task is meant to detect an interrupt
    def task(self):
        if self.int1_pin.value() == 0:
            if Accel_Config.check_interrupt_z_axis():
                return True
        return False

    def set_params(self, enable, new_accel_threshold_mg):
        if Accel_Config.set_interrupt_threshold(new_accel_threshold_mg):
            self.accel_threshold_interrupt_mg = new_accel_threshold_mg
            self.enabled = bool(enable)
            Accel_Config.enable_disable(bool(enable))
            static_mem_save_accel_params(self.enabled, self.accel_threshold_interrupt_mg)
            return True
        return False

    def get_params(self):
        return [self.enabled, self.accel_threshold_interrupt_mg]

    @staticmethod
    def get_temperature():
        return Accel_Config.get_temperature()




